"use strict";
export class Controller {
  repository;
  entity;
  mapper;
  validate;

  constructor(entity, repository, mapper, validate) {
    this.repository = repository;
    this.mapper = mapper;
    this.entity = entity;
    this.validate = validate;
  }

  setError = (res, error, func) => {
    console.error(error);
    res.status(500);
    res.json({ message: error.message, func: func });
  };

  setOk = (res, status, data) => {
    res.status(status || 200);
    res.send(data);
  };

  getParam(req, name) {
    return req.params[name];
  }

  getRepository() {
    return this.repository;
  }

  #validate(data) {
    for (let item in data) {
      if (!item) {
        throw new Error(
          `El campo ${item} no está informado en el controller ${entity}`
        );
      }
    }
  }

  async query(req, res, loc, func) {
    try {
      this.setOk(res, 200, await func(req));
    } catch (error) {
      this.setError(res, error, loc);
    }
  }

  async getAll(req, res) {
    await this.query(
      req,
      res,
      "getAll",
      this.repository.getAll.bind(this.repository)
    );
  }
  async getById(req, res, next) {
    const id = this.getParam(req, "id");
    await this.query(
      id,
      res,
      "getById",
      this.repository.getById.bind(this.repository)
    );
  }

  #getMessage(loc, affectedRows) {
    return {
      message: `${entity} (${loc})`,
      items: affectedRows[0].affectedRows,
    };
  }

  async change(data, res, loc, func, id) {
    try {
      let result;
      if (data && id) {
        result = await func(id, data);
      } else if (data) {
        result = await func(data);
      } else {
        result = await func(id);
      }
      this.setOk(res, 200, {
        message: `${this.entity} (${loc})`,
        items: result[0].affectedRows,
      });
    } catch (error) {
      this.setError(res, error, loc);
    }
  }

  async insert(req, res) {
    try {
      const data = this.mapper ? this.mapper(req.body) : req.body;
      this.validate ? this.validate(data) : this.#validate(data);
      await this.change(
        data,
        res,
        "add",
        this.repository.insert.bind(this.repository)
      );
    } catch (error) {
      this.setError(res, error, "add");
    }
  }
  async update(req, res) {
    try {
      const id = this.getParam(req, "id");
      const data = this.mapper ? this.mapper(req.body) : req.body;
      this.validate ? this.validate(data) : this.#validate(data);
      await this.change(
        data,
        res,
        "update",
        this.repository.update.bind(this.repository),
        id
      );
    } catch (error) {
      this.setError(res, error, "update");
    }
  }
  async delete(req, res, next) {
    const id = this.getParam(req, "id");
    await this.change(
      undefined,
      res,
      "delete",
      this.repository.delete.bind(this.repository),
      id
    );
  }
}
