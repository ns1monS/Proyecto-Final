"use strict";
import AuthError from "../errors/authError.js";
import LoginError from "../errors/loginError.js";
// crear nueva clase de error en js

const controller = function (entity_, repository_, mapper_, validation_) {
  const repository = repository_;
  const mapper = mapper_;
  const entity = entity_;
  const validation = validation_;

  function setError(res, error, func) {
    console.error(error);
    if (error instanceof LoginError) {
      res.status(401);
    } else if (error instanceof AuthError) {
      res.status(405);
    } else {
      res.status(500);
    }
    res.json({ message: error.message, func: func });
  }

  function setOk(res, status, data) {
    res.status(status || 200);
    res.send(data);
  }

  function validate(data) {
    for (let item in data) {
      if (!item) {
        throw new Error(
          `El campo ${item} no está informado en el controller ${entity}`
        );
      }
    }
  }

  function checkAuth(req) {
    const { authorization } = req.headers;
    // Si no existe el token enviamos un 401 (unauthorized)
    if (!authorization) throw new AuthError();
  }

  function getParam(req, name) {
    return req.params[name];
  }

  async function query(req, res, loc, func) {
    try {
      setOk(res, 200, await func(req));
    } catch (error) {
      setError(res, error, loc);
    }
  }

  async function getAll(req, res) {
    await query(req, res, "getAll", repository.getAll);
  }

  async function getById(req, res, next) {
    const id = getParam(req, "id");
    await query(id, res, "getById", repository.getById);
  }

  function getMessage(loc, affectedRows) {
    return {
      message: `${entity} (${loc})`,
      items: affectedRows[0].affectedRows,
    };
  }

  async function change(data, res, loc, func, id) {
    try {
      let result;
      if (data && id) {
        result = await func(id, data);
      } else if (data) {
        result = await func(data);
      } else {
        result = await func(id);
      }
      setOk(res, 200, {
        message: `${entity} (${loc})`,
        items: result[0].affectedRows,
      });
    } catch (error) {
      setError(res, error, loc);
    }
  }

  async function ins(req, res) {
    try {
      const data = mapper ? mapper(req.body) : req.body;
      validation ? validation(data) : validate(data);
      await change(data, res, "add", repository.ins);
    } catch (error) {
      setError(res, error, "add");
    }
  }

  async function upd(req, res) {
    try {
      const id = getParam(req, "id");
      const data = mapper ? mapper(req.body) : req.body;
      validation ? validation(data) : validate(data);
      await change(data, res, "update", repository.upd, id);
    } catch (error) {
      setError(res, error, "update");
    }
  }

  async function del(req, res, next) {
    const id = getParam(req, "id");
    await change(undefined, res, "delete", repository.del, id);
  }

  return {
    setOk,
    setError,
    getParam,
    change,
    query,
    getAll,
    getById,
    ins,
    upd,
    del,
  };
};

export default controller;
