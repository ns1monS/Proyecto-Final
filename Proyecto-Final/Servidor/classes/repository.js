"use strict";

import { getConnection } from "../utils/database.js";

const repository = function (table_, key_) {
  const table = table_;
  const key = key_ || "id";
  const status = {
    error: null,
    message: null,
    fields: null,
  };

  const qInsert = `insert into ${table} set ?`;
  const qDelete = `delete from ${table} where ${key}=?`;
  const qUpdate = `update ${table} set ? where ${key}=?`;
  const qSelect = `select * from ${table} where ${key}=?`;
  const qSelectAll = `select * from ${table}`;
  const infoTable = ` tabla ${table}`;

  async function openConnection() {
    setStatus();
    const temp = await getConnection();
    if (temp && temp.off) await temp.connect();
    return temp;
  }
  async function closeConnection(connection) {
    // todo: pendiente de ver como cerrar conexiones y/o usar un pool
    // connection && (await connection.end());
  }

  function setStatus(error, message) {
    status.error = error;
    status.message = message;
    status.fields = null;
    return status;
  }

  function setOk(fields) {
    status.error = null;
    status.message = null;
    status.fields = fields;
    return status;
  }

  function infoKey(id) {
    return ` tabla ${table} y pk ${key}: ${id}`;
  }

  async function executeQuery(queryString, parameters, name = "executeQuery") {
    let connection;
    try {
      connection = await openConnection();
      const [rows, fields] = await connection.query(queryString, parameters);
      setOk(fields);
      return rows;
    } catch (error) {
      const out = setStatus(
        error,
        `Error en método ${name} ${queryString} con parámetros ${JSON.stringify(
          parameters
        )} en la tabla ${infoTable}`
      );
      console.log(out);
      throw error;
    } finally {
      await closeConnection(connection);
    }
  }

  async function executeDML(queryString, parameters, name = "executeDML") {
    let connection;
    try {
      connection = await openConnection();
      const result = await connection.query(queryString, parameters);
      setStatus(0, `Filas afectadas: ${result[0].affectedRows}`);
      return result;
    } catch (error) {
      const out = setStatus(
        error,
        `Error en método ${name} ${queryString} con parámetros ${JSON.stringify(
          parameters
        )} en la tabla ${infoTable}`
      );
      console.log(out);
      throw error;
    } finally {
      await closeConnection(connection);
    }
  }

  return {
    executeDML,
    executeQuery,
    getStatus() {
      return status;
    },
    async getAll() {
      return await executeQuery(qSelectAll, undefined, "getAll");
    },

    async getById(id) {
      return await executeQuery(qSelect, id, "getById");
    },
    async upd(id, data) {
      return await executeDML(qUpdate, [data, id], "update");
    },
    async ins(data) {
      return await executeDML(qInsert, [data], "insert");
    },
    async del(id) {
      return await executeDML(qDelete, [id], "delete");
    },
  };
};
export default repository;
