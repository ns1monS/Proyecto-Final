import { getConnection } from "../utils/database.js";

export class Repository {
  table;
  key;
  status = {
    error: undefined,
    message: "",
  };
  #qInsert;
  #qDelete;
  #qUpdate;
  #qSelect;
  #qSelectAll;
  #infoTable;
  #fields;

  constructor(table, key) {
    this.table = table;
    this.key = key || "id";
    this.#qInsert = `insert into ${this.table} set ?`;
    this.#qDelete = `delete from ${this.table} where ${this.key}=?`;
    this.#qUpdate = `update ${this.table} set ? where ${this.key}=?`;
    this.#qSelect = `select * from ${this.table} where ${this.key}=?`;
    this.#qSelectAll = `select * from ${this.table}`;
    this.#infoTable = ` tabla ${this.table}`;
  }

  async #getConnection() {
    this.#setStatus(undefined, "");
    const temp = await getConnection();
    if (temp && temp.off) await temp.connect();
    return temp;
  }

  async #closeConnection(connection) {
    // todo: pendiente de ver como cerrar conexiones y/o usar un pool
    // connection && (await connection.end());
  }

  #setStatus(error, message) {
    this.status.error = error;
    this.status.message = message;
    return this.status;
  }

  getStatus() {
    return this.status;
  }

  #infoKey(id) {
    return ` tabla ${this.table} y pk ${this.key}: ${id}`;
  }

  async executeQuery(queryString, parameters, name = "executeQuery") {
    let connection;
    try {
      connection = await this.#getConnection();
      const [rows, fields] = await connection.query(queryString, parameters);
      this.fields = fields;
      return rows;
    } catch (error) {
      const out = this.#setStatus(
        error,
        `Error en método ${name} ${queryString} con parámetros ${JSON.stringify(
          parameters
        )} en la tabla ${this.#infoTable}`
      );
      console.log(out);
      throw error;
    } finally {
      await this.#closeConnection(connection);
    }
  }

  async executeDML(queryString, parameters, name = "executeDML") {
    let connection;
    try {
      connection = await this.#getConnection();
      const result = await connection.query(queryString, parameters);
      this.#setStatus(0, `Filas afectadas: ${result[0].affectedRows}`);
      return result;
    } catch (error) {
      const out = this.#setStatus(
        error,
        `Error en método ${name} ${queryString} con parámetros ${JSON.stringify(
          parameters
        )} en la tabla ${this.#infoTable}`
      );
      console.log(out);
      throw error;
    } finally {
      await this.#closeConnection(connection);
    }
  }

  getFields() {
    return this.#fields;
  }

  async getAll() {
    return await this.executeQuery(this.#qSelectAll, undefined, "getAll");
  }

  async getById(id) {
    return await this.executeQuery(this.#qSelect, id, "getById");
  }

  async update(id, data) {
    return await this.executeDML(this.#qUpdate, [data, id], "update");
  }
  async insert(data) {
    return await this.executeDML(this.#qInsert, [data], "insert");
  }
  async delete(id) {
    return await this.executeDML(this.#qDelete, [id], "delete");
  }
}
