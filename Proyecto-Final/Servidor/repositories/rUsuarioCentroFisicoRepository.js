import Repository from "../classes/repository.js";
const name = "rUsuarioCentroFisico";

const newRepository = function () {
  const repository = Repository(name);
  return Object.assign({}, repository, {});
};

export default newRepository;
