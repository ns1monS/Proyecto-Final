import Repository from "../classes/repository.js";
const name = "recurso";

const newRepository = function () {
  const repository = Repository(name);
  return Object.assign({}, repository, {});
};

export default newRepository;
