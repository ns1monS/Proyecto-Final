import Repository from "../classes/repository.js";
const tabla = "centroFisico";

const newRepository = function () {
  const repository = Repository(tabla);
  return Object.assign({}, repository, {});
};

export default newRepository;
