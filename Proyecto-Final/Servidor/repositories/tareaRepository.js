import Repository from "../classes/repository.js";
const name = "tarea";
const selectByContacto = `SELECT * FROM ${name} WHERE contacto =?`;

const newRepository = function () {
  const repository = Repository(name);
  return Object.assign({}, repository, {
    async getByContacto(contacto) {
      return await repository.executeQuery(selectByContacto, contacto);
    },
  });
};

export default newRepository;
