import Repository from "../classes/repository.js";
const tabla = "centro";
const selectByCodigo = "select * from centro where codigo = ?";
const selectByNombre = "select * from centro where nombre = ?";

const newRepository = function () {
  const repository = Repository(tabla);
  return Object.assign({}, repository, {
    async getByCodigo(codigo) {
      return await repository.executeQuery(selectByCodigo, codigo);
    },
    async getByNombre(nombre) {
      return await repository.executeQuery(selectByNombre, nombre);
    },
  });
};

export default newRepository;
