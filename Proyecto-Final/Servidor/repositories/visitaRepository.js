import Repository from "../classes/repository.js";
const tabla = "visita";
const visitaMotivo = `Select visita.usuario , motivo.nombre , 
centroFisico.nombre , visita.fecha from visita join centroFisico on visita.centroFisico = centroFisico.id
join motivo on visita.motivo = motivo.id`;

const selectByUsuario = "SELECT * FROM VISITA WHERE usuario = ?";

const newRepository = function () {
  const repository = Repository(tabla);
  async function getVisita(visita) {
    return await repository.executeQuery(visitaMotivo, visita);
  }
  return Object.assign({}, repository, {
    getVisita,
  });
};

export default newRepository;
