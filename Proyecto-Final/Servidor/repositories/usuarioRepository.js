import Repository from "../classes/repository.js";
const tabla = "usuario";
const selectByEmail = "SELECT * FROM usuario WHERE email = ?";

const newRepository = function () {
  const repository = Repository(tabla);
  return Object.assign({}, repository, {
    async getByEmail(email) {
      return await repository.executeQuery(selectByEmail, email);
    },
  });
};

export default newRepository;
