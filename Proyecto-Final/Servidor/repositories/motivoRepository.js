import Repository from "../classes/repository.js";
const name = "motivo";
const selectByNombre = "SELECT FROM MOTIVO WHERE nombre = ?";

const newRepository = function () {
  const repository = Repository(name);
  return Object.assign({}, repository, {});
};

export default newRepository;
