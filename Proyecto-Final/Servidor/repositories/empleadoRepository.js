import Repository from "../classes/repository.js";
const tabla = "empleado";

const selectByNif = "SELECT * FROM empleado WHERE nif = ?";

const newRepository = function () {
  const repository = Repository(tabla);
  return Object.assign({}, repository, {
    async getByNif(nif) {
      return await repository.executeQuery(selectByNif, nif);
    },
  });
};

export default newRepository;
