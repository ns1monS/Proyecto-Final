import Repository from "../classes/repository.js";
const tabla = "contacto";
const contactosByEmpleado =
  "select * from contacto join empleado on contacto.empleado = empleado.id where empleado.id = ?";

const newRepository = function () {
  const repository = Repository(tabla);
  async function getByEmpleado(empleado) {
    return await repository.executeQuery(contactosByEmpleado, empleado);
  }
  return Object.assign({}, repository, {
    getByEmpleado,
  });
};

export default newRepository;
