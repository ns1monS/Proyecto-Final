import { config } from "dotenv";

config();

export default {
  host: process.env.DB_HOST || "localhost",
  database: process.env.DB_NAME || "",
  user: process.env.DB_USER || "",
  password: process.env.DB_PASS || "",
  useJwt: process.env.USE_JWT,
};
