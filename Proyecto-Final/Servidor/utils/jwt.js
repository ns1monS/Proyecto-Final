import { SignJWT, jwtVerify } from "jose";
import AuthError from "../errors/authError.js";
import LoginError from "../errors/loginError.js";
import config from "./config.js";

const verifyToken = async (req, res, next) => {
  if (!config.useJwt) {
    next();
    return;
  }
  const { authorization } = req.headers;
  if (!authorization) throw new LoginError();

  // const token = authorization.split(" ")[1];
  const encoder = new TextEncoder();

  const { payload } = await jwtVerify(
    authorization,
    encoder.encode(process.env.JWT_SECRET)
  );
  if (!payload.id || payload.email) throw new AuthError();
  next();
};

const buildToken = async (usuario) => {
  const encoder = new TextEncoder();
  const jwtConstructor = new SignJWT({
    id: usuario.id,
    email: usuario.email,
  });
  return await jwtConstructor
    .setProtectedHeader({ alg: "HS256", typ: "JWT" })
    .setIssuedAt()
    .setExpirationTime("1h")
    .sign(encoder.encode(process.env.JWT_SECRET));
};
export { verifyToken, buildToken };
