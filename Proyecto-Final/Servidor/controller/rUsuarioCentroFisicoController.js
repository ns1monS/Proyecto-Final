import Controller from "../classes/controller.js";
import Repository from "../repositories/rUsuarioCentroFisicoRepository.js";
const name = "rUsuarioCentroFisico";

const newController = function (repository, mapper, validation) {
  repository = repository || new Repository();
  const controller = Controller(name, repository, mapper, validation);
  return Object.assign({}, controller, {});
};

export default newController;
