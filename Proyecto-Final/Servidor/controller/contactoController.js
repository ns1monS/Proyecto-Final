import Controller from "../classes/controller.js";
import Repository from "../repositories/contactoRepository.js";
const name = "contacto";

const newController = function (repository, mapper, validation) {
  repository = repository || new Repository();
  const controller = Controller(name, repository, mapper, validation);
  async function getByEmpleado(req, res) {
    const empleado = controller.getParam(req, "id");
    await controller.query(
      empleado,
      res,
      "getByEmpleado",
      repository.getByEmpleado
    );
  }
  return Object.assign({}, controller, {
    getByEmpleado,
  });
};

export default newController;
