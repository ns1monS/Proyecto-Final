import Controller from "../classes/controller.js";
import Repository from "../repositories/recursoRepository.js";
const name = "recurso";

const newController = function (repository, mapper, validation) {
  repository = repository || new Repository();
  const controller = Controller(name, repository, mapper, validation);
  return Object.assign({}, controller, {});
};

export default newController;
