import md5 from "md5";
import { buildToken } from "../utils/jwt.js";
import Controller from "../classes/controller.js";
import Repository from "../repositories/usuarioRepository.js";
const name = "usuario";

const newController = function (repository, mapper, validation) {
  repository = repository || new Repository();
  const controller = Controller(name, repository, mapper, validation);

  async function login(req, res) {
    const { email, password } = req.body;
    try {
      const usuario = await repository.getByEmail(email);
      if (usuario.length === 0) throw new Error("Usuario ya registrado");
      if (usuario[0].password !== md5(password))
        throw new Error("Password incorrecta");
      controller.setOk(res, 200, await buildToken(usuario[0]));
    } catch (error) {
      controller.setError(res, error, "login");
    }
  }

  async function register(req, res) {
    const { email, password } = req.body;
    try {
      const user = await repository.getByEmail(email);
      req.body.password = md5(password);
      if (user.length > 0) throw new Error("Usuario ya registrado");
      await controller.ins(req, res);
    } catch (error) {
      controller.setError(res, error, "register");
    }
  }
  async function upd(req, res) {
    const { password } = req.body;
    try {
      req.body.password = md5(password);
      await controller.upd(req, res);
    } catch (error) {
      controller.setError(res, error, "upd");
    }
  }

  return Object.assign({}, controller, {
    login,
    register,
    upd,
  });
};

export default newController;
