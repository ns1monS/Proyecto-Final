import Controller from "../classes/controller.js";
import Repository from "../repositories/centroFisicoRepository.js";
const name = "centroFisico";

const newController = function (repository, mapper, validation) {
  repository = repository || new Repository();
  const controller = Controller(name, repository, mapper, validation);
  return Object.assign({}, controller, {});
};

export default newController;
