import Controller from "../classes/controller.js";
import Repository from "../repositories/empleadoRepository.js";
const name = "empleado";

const newController = function (repository, mapper, validation) {
  repository = repository || new Repository();
  const controller = Controller(name, repository, mapper, validation);

  async function getByNif(req, res) {
    const nif = controller.getParam(req, "id");
    await controller.query(nif, res, "getByNif", repository.getByNif);
  }

  async function register(req, res) {
    const { nif } = req.body;
    try {
      const empleado = await repository.getByNif(nif);
      if (empleado.length > 0) throw new Error("Empleado ya existe");

      await controller.ins(req, res);
    } catch (error) {
      controller.setError(res, error, "register");
    }
  }
  return Object.assign({}, controller, {
    getByNif,
    register,
  });
};

export default newController;
