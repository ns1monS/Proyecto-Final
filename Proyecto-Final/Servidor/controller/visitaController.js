import Controller from "../classes/controller.js";
import Repository from "../repositories/visitaRepository.js";
const name = "visita";

const newController = function (repository, mapper, validation) {
  repository = repository || new Repository();
  const controller = Controller(name, repository, mapper, validation);
  async function getVisita(req, res) {
    const visita = controller.getParam(req, "id");
    controller.query(visita, res, "getVisita", repository.getVisita);
  }

  return Object.assign({}, controller, {
    getVisita,
  });
};

export default newController;
