import Controller from "../classes/controller.js";
import Repository from "../repositories/tareaRepository.js";
const name = "tarea";

const newController = function (repository, mapper, validation) {
  repository = repository || new Repository();
  const controller = Controller(name, repository, mapper, validation);

  async function getByContacto(req, res) {
    const contacto = controller.getParam(req, "id");
    await controller.query(
      contacto,
      res,
      "getByContacto",
      repository.getByContacto
    );
  }

  async function postTareas(req, res) {
    try {
      const tarea = await repository.getByContacto(contacto);
      await controller.ins(req, res);
    } catch (error) {
      controller.setError(res, error, "tarea");
    }
  }

  return Object.assign({}, controller, {
    getByContacto,
    postTareas,
  });
};

export default newController;
