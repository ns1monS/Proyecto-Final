import express from "express";
import dotenv from "dotenv";
import logger from "morgan";
import cookieParser from "cookie-parser";
import usuarioRouter from "./routes/usuarioRouter.js";
import empleadoRouter from "./routes/empleadoRouter.js";
import centroRouter from "./routes/centroRouter.js";
import centroFisicoRouter from "./routes/centroFisicoRouter.js";
import contactoRouter from "./routes/contactoRouter.js";
import motivoRouter from "./routes/motivoRouter.js";
import recursoRouter from "./routes/recursoRouter.js";
import rUsuarioCentroFisicoRouter from "./routes/rUsuarioCentroFisicoRouter.js";
import visitaRouter from "./routes/visitaRouter.js";
import tareaRouter from "./routes/tareaRouter.js";
import cors from "cors";
import { fileURLToPath } from "url";
import { dirname, join } from "path";

dotenv.config();
export function currentDir() {
  const __filename = fileURLToPath(import.meta.url);
  const __dirname = dirname(__filename);
  return { __dirname, __filename };
}
const { __dirname } = currentDir();

const app = express();

app.use(express.json());
app.use(express.text());
app.use(cors());
app.use(logger("dev"));
app.use(cookieParser());

app.use("/usuario", usuarioRouter);
app.use("/empleado", empleadoRouter);
app.use("/centro", centroRouter);
app.use("/centroFisico", centroFisicoRouter);
app.use("/contacto", contactoRouter);
app.use("/motivo", motivoRouter);
app.use("/recurso", recursoRouter);
app.use("/rUsuarioCentroFisico", rUsuarioCentroFisicoRouter);
app.use("/tarea", tareaRouter);
app.use("/visita", visitaRouter);

// app.use("/visita", );

export default app;
