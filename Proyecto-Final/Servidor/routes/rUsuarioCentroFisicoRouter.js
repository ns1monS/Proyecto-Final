import express from "express";
import Controller from "../controller/rUsuarioCentroFisicoController.js";
import { verifyToken } from "../utils/jwt.js";

const router = express.Router();
const controller = new Controller();

router.use(verifyToken);

router.get("/", controller.getAll);
router.get("/:id", controller.getById);
router.post("/", controller.ins);
router.delete("/:id", controller.del);
router.put("/:id", controller.upd);

export default router;
