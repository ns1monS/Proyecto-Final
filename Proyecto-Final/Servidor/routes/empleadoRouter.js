import express from "express";
import Controller from "../controller/empleadoController.js";
import { verifyToken } from "../utils/jwt.js";

const router = express.Router();
const controller = new Controller();

router.use(verifyToken);

router.get("/", controller.getAll);
router.post("/", controller.ins);
router.post("/register", controller.register);

router.get("/nif/:id", controller.getByNif);

router.get("/:id", controller.getById);

router.delete("/:id", controller.del);
router.put("/:id", controller.upd);

export default router;
