import express from "express";
import Controller from "../controller/usuarioController.js";
import validateLoginDto from "../utils/validateLoginDto.js";
import { verifyToken } from "../utils/jwt.js";

const controller = new Controller();
const router = express.Router();

router.post("/", controller.register);
router.post("/login", validateLoginDto, controller.login);
router.delete("/:id", verifyToken, controller.del);
router.patch("/:id", verifyToken, controller.upd);

export default router;
