import express from "express";
import { verifyToken } from "../utils/jwt.js";
import Controller from "../controller/visitaController.js";

const router = express.Router();
const controller = new Controller();

router.use(verifyToken);
router.get("/", controller.getAll);
router.post("/", controller.ins);
router.get("/:id", controller.getById);
router.get("/visita/", controller.getVisita);
router.delete("/:id", controller.del);
router.put("/:id", controller.upd);

export default router;
