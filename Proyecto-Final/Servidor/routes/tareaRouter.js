import express from "express";
import Controller from "../controller/tareaController.js";
import { verifyToken } from "../utils/jwt.js";

const router = express.Router();
const controller = new Controller();

router.use(verifyToken);

router.get("/", controller.getAll);
router.get("/:id", controller.getById);
router.get("/contacto/:id", controller.getByContacto);
router.post("/", controller.ins);
router.post("/contacto", controller.postTareas);
router.delete("/:id", controller.del);
router.put("/:id", controller.upd);

export default router;
