import db from "../services/mysql.js";

const empleadoQueries = {};

empleadoQueries.getEmpleadobyNif = async (nif) => {
  try {
    return await db.query("SELECT * FROM empleado WHERE nif = ?", nif);
  } catch (e) {
    throw new Error(e);
  } finally {
    console.log(db);
  }
};

empleadoQueries.addEmpleado = async (empleadoData) => {
  // Conectamos con la base de datos y añadimos el empleado.
  try {
    conn = await db.createConnection();
    // Creamos un objeto con los datos del empleado a guardar en la base de datos.
    // usamos la libreria momentjs para registrar la fecha actual
    let userObj = {
      nombre: empleadoData.nombre,
      apellidos: empleadoData.apellidos,
      telefono: empleadoData.telefono,
      nif: empleadoData.nif,
    };
    return await db.query(
      "INSERT INTO empleado SET ?",
      userObj,
      "insert",
      conn
    );
  } catch (e) {
    throw new Error(e);
  } finally {
    conn && (await conn.end());
  }
};

export default empleadoQueries;
