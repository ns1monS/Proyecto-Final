import dao from "../../services/dao.js";
import empleadoRepository from "../../repositories/empleadoRepository.js";
import centroRepository from "../../repositories/centroRepository.js";

const controller = {};

controller.addEmpleado = async (req, res) => {
  const { nombre, apellidos, telefono, nif, centro } = req.body;
  // Si no alguno de estos campos recibidos por el body devolvemos un 400 (bad request)
  if (!nombre || !apellidos || !telefono || !nif || !centro)
    return res.status(400).send("Error al recibir el body");
  // Buscamos el empleado en la base de datos
  try {
    const empleado = await empleadoRepository.getByNif(nif);
    if (empleado?.length > 0) {
      return res.status(409).send("empleado ya registrado");
    }
    const centro = centroRepository.insert();

    const addEmpleado = await empleadoRepository.insert(req.body);
    if (addEmpleado)
      return res.send(`empleado ${nombre} con id: ${addEmpleado} registrado`);

    // const user = await dao.getEmpleadobyNif(nif);
    // // Si existe el empleado respondemos con un 409 (conflict)
    // if (user.length > 0) return res.status(409).send("empleado ya registrado");
    // // Si no existe lo registramos
    // const addEmpleado = await dao.addEmpleado(req.body);
    // if (addEmpleado)
    //   return res.send(`empleado ${nombre} con id: ${addEmpleado} registrado`);
  } catch (e) {
    console.log(e.message);
  }
};

controller.getEmpleado = async (req, res) => {
  console.log(req);
  const nif = req.params.id;
  try {
    const empleado = await empleadoRepository.getByNif(nif);
    console.log(empleado);
    // const user = await dao.getEmpleadobyNif(nif);
    if (empleado.length <= 0)
      return res.status(404).send("EL empleado no existe");
    return res.send(empleado[0]);
  } catch (e) {
    console.log(e.message);
    return res.status(400).send(e.message);
  }
};

export default controller;
