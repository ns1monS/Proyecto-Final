import userQueries from "../mysql_queries/user_queries.js";
import empleadoQueries from "../mysql_queries/empleado_queries.js";
const dao = {};

// Buscar un usuario por el email
dao.getUserbyEmail = async (email) => await userQueries.getUserbyEmail(email);
// Añadir un nuevo usuario
dao.addUser = async (userData) => await userQueries.addUser(userData);

// Buscar un usuario por el id
dao.getUserbyId = async (id) => await userQueries.getUserbyId(id);

// Eliminar un usuario
dao.deleteUser = async (id) => await userQueries.deleteUser(id);
// Modificar usuario por su id
dao.updateUser = async (id, userData) =>
  await userQueries.updateUser(id, userData);

// Buscar un empleado por el nif
dao.getEmpleadobyNif = async (nif) =>
  await empleadoQueries.getEmpleadobyNif(nif);

// Crear un nuevo empleado en al base de datos
dao.addEmpleado = async (empleadoData) =>
  await empleadoQueries.addEmpleado(empleadoData);

export default dao;
