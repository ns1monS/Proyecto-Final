import { createContext, useState, useContext, useMemo } from "react";
import MY_AUTH_APP from "../AuthContext/AuthApp";
import jwtDecode from "jwt-decode";
const AuthContext = createContext({
  authorization: {
    email: null,
    id: null,
  },
  login: () => {},
  logout: () => {},
});
export default AuthContext;
export function AuthContextProvider({ children }) {
  const [authorization, setAuthorization] = useState(
    JSON.parse(window.localStorage.getItem(MY_AUTH_APP)) ?? {
      email: null,
      id: null,
    }
  );

  async function login(values) {
    const response = await fetch(`http://localhost:3000/usuario/login`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(values),
    });
    if (response.status === 200) {
      const token = await response.text();
      console.log(token);
      setAuthorization(jwtDecode(token));
      window.localStorage.setItem(
        MY_AUTH_APP,
        JSON.stringify(jwtDecode(token))
      );
    } else {
      alert("Por favor introduzca los datos correctos");
      console.log("error");
    }
  }
  function logout() {
    window.localStorage.removeItem(MY_AUTH_APP);
    setAuthorization({
      email: null,
      id: null,
    });
    {
      login ? "Logout" : login;
    }
  }
  const value = useMemo(
    () => ({
      authorization,
      login,
      logout,
    }),
    [authorization, login, logout]
  );
  return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
}
export function useAuthContext() {
  return useContext(AuthContext);
}
