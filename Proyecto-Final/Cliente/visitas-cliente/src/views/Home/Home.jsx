import "./Home.css";
import Header from "../../components/Header/Header";
import Footer from "../../components/Footer/Footer";
import CentroMotivo from "../CentroMotivo/CentroMotivo";
export default function Home() {
  return (
    <>
      <div className="header p-0">
        <Header />
      </div>
      <div className="main">
        <h4> Seleccione motivo y centro de su visita</h4>
      </div>
      <div className="main">
        <CentroMotivo />
      </div>
      <div className="footer ">
        <Footer />
      </div>
    </>
  );
}
