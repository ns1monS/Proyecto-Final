import TodoList from "../../components/Tareas/Tareas";
import Header from "../../components/Header/Header";
import Footer from "../../components/Footer/Footer";
export default function Tareas() {
  return (
    <>
      <Header />
      <TodoList />
      <Footer />
    </>
  );
}
