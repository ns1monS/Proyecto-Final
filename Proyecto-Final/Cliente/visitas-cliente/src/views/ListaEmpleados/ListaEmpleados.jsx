import Empleados from "../../components/ListaEmpleados/ListaEmpleados";
import Header from "../../components/Header/Header";
import Footer from "../../components/Footer/Footer";
export default function ListaEmpleados() {
  return (
    <>
      <div>
        <div>
          <h1>Lista de empleados</h1>
        </div>
        <Empleados />
        <Footer />
      </div>
    </>
  );
}
