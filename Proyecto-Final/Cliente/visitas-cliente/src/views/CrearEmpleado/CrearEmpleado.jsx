import Registro from "../../components/RegistrarEmpleado/RegistrarEmpleado";
import Header from "../../components/Header/Header";
import Footer from "../../components/Footer/Footer";
export default function CrearEmpleado() {
  return (
    <>
      <Header />
      <Registro />
      <Footer />
    </>
  );
}
