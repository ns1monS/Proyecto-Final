import ModificarEmpleado from "../../components/ModificarEmpleado/ModificarEmpleado";
import Footer from "../../components/Footer/Footer";
export default function ModEmpleado() {
  return (
    <>
      <ModificarEmpleado />
      <Footer />
    </>
  );
}
