import "./App.css";
import Login from "./views/Login/Login";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { AuthContextProvider } from "./context/AuthContext/AuthContext";
import Layout from "./components/Layout/Layout";
import Home from "./views/Home/Home";
import RegistrarEmpleado from "./views/RegistrarEmpleado/RegistrarEmpleado";
import ModEmpleado from "./views/ModEmpleado/ModEmpleado";
import ListaEmpleados from "./views/ListaEmpleados/ListaEmpleados";
import CrearEmpleado from "./views/CrearEmpleado/CrearEmpleado";
import Contacto from "./components/Contacto/Contacto";
import Tareas from "./views/Tareas/Tareas";
function App() {
  return (
    <AuthContextProvider>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Layout />}>
            <Route index element={<Login />} />
            <Route path="/Home" element={<Home />} />
            <Route path="/UpdateE" element={<RegistrarEmpleado />} />
            <Route path="/ListaEmpleados" element={<ListaEmpleados />} />
            <Route path="/ModificarEmpleado/:id" element={<ModEmpleado />} />
            <Route path="/CrearEmpleado" element={<CrearEmpleado />} />
            <Route path="/Contacto/:id" element={<Contacto />} />
            <Route path="/Tareas/:id" element={<Tareas />} />
          </Route>
        </Routes>
      </BrowserRouter>
    </AuthContextProvider>
  );
}

export default App;
