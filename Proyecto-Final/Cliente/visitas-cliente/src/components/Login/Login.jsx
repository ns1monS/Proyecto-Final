import { useFormik } from "formik";
import { BasicFormSchema } from "./BasicFormSchema";
import { useNavigate } from "react-router-dom";
import { useAuthContext } from "../../context/AuthContext/AuthContext";
import { useEffect } from "react";
import "./Login.css";

export default function LoginUser() {
  const { authorization, login } = useAuthContext();
  const navigate = useNavigate();
  async function onSubmit(values, actions) {
    login(values);
    await new Promise((resolve) => setTimeout(resolve, 2000));
  }

  const {
    values,
    touched,
    errors,
    handleBlur,
    handleChange,
    handleSubmit,
    isSubmitting,
  } = useFormik({
    initialValues: { email: "", password: "" },
    validationSchema: BasicFormSchema,
    onSubmit,
  });
  useEffect(() => {
    if (authorization.email) {
      navigate("/Home");
    }
  }, [authorization]);

  return (
    <div>
      <div className="container justify-content-center align-items-center">
        <div>
          <div className="py-4 text-lg-center">
            <h2>Inicia sesion</h2>
          </div>
        </div>
        <div className="d-flex ">
          <form
            className="needs-validation justify-content-center"
            onSubmit={handleSubmit}
          >
            <div className="row g-3 d-flex justify-content-center ">
              <div className="col-sm-6  " id="inputGroup-sizing-sm">
                <input
                  type="email"
                  name="email"
                  value={values.email}
                  onChange={handleChange}
                  className={
                    errors.email && touched.email
                      ? "form-control is-invalid"
                      : "form-control"
                  }
                  id="email"
                  placeholder="Email"
                  aria-describedby="inputGroup-sizing-sm"
                  onBlur={handleBlur}
                />
                <div
                  className={
                    errors.email && touched.email
                      ? "invalid-feeback is-invalid"
                      : ""
                  }
                >
                  {errors.email}
                </div>
              </div>
              <div className="col-sm-6">
                <input
                  type="password"
                  className={
                    errors.password && touched.password
                      ? "form-control is-invalid"
                      : "form-control"
                  }
                  name="password"
                  value={values.password}
                  onChange={handleChange}
                  placeholder="write a password"
                  onBlur={handleBlur}
                  aria-describedby="inputGroup-sizing-sm"
                />
                <div
                  className={
                    errors.password && touched.password
                      ? "invalid-feeback is-invalid"
                      : ""
                  }
                >
                  {errors.password}
                </div>{" "}
              </div>

              <div className="my-3"></div>
            </div>
            <button
              className="btn btn-outline-success"
              type="submit"
              disabled={isSubmitting}
            >
              Iniciar sesión
            </button>
          </form>
        </div>
      </div>
    </div>
  );
}
