import { useState, useEffect } from "react";
import { useFormik } from "formik";
import axios from "axios";
import { useNavigate } from "react-router-dom";

// export function CentroFisico() {
//   const [opciones, setOpciones] = useState([]);

//   useEffect(() => {
//     axios
//       .get("http://localhost:3000/centroFisico/")
//       .then((response) => {
//         setOpciones(response.data);
//       })
//       .catch((error) => {
//         console.error(error);
//       });
//   }, []);

//   const formik = useFormik({
//     initialValues: {
//       selectedOpcion: "",
//     },
//   });

//   return (
//     <form onSubmit={formik.handleSubmit}>
//       <label htmlFor="selectedOpcion">Selecciona un centro físico:</label>
//       <select
//         id="selectedOpcion"
//         name="selectedOpcion"
//         value={formik.values.selectedOpcion}
//         onChange={formik.handleChange}
//       >
//         {opciones.map((opcion) => (
//           <opcion key={opcion.id} value={opcion.value}>
//             {opcion.nombre}
//           </opcion>
//         ))}
//       </select>
//     </form>
//   );
// }

// export function Motivo() {
//   const [options, setOptions] = useState([]);

//   useEffect(() => {
//     axios
//       .get("http://localhost:3000/motivo/")
//       .then((response) => {
//         setOptions(response.data);
//       })
//       .catch((error) => {
//         console.error(error);
//       });
//   }, []);

//   const formik = useFormik({
//     initialValues: {
//       selectedOption: "",
//     },
//   });

//   return (
//     <form onSubmit={formik.handleSubmit}>
//       <label htmlFor="selectedOption">Selecciona un motivo:</label>
//       <select
//         id="selectedOption"
//         name="selectedOption"
//         value={formik.values.selectedOption}
//         onChange={formik.handleChange}
//       >
//         {options.map((option) => (
//           <option key={option.id} value={option.value}>
//             {option.nombre}
//           </option>
//         ))}
//       </select>
//     </form>
//   );
// }

const getMotivo = () => {
  return axios.get("http://localhost:3000/motivo/");
};

const getCentroFisico = () => {
  return axios.get("http://localhost:3000/centroFisico/");
};

export default function PostForm() {
  const [lastVisitId, setLastVisitId] = useState(null);
  const [motivoData, setMotivoData] = useState([]);
  const [centroFisicoData, setCentroFisicoData] = useState([]);
  const navigate = useNavigate();

  useEffect(() => {
    getMotivo().then((response) => setMotivoData(response.data));
    getCentroFisico().then((response) => setCentroFisicoData(response.data));
  }, []);

  const formik = useFormik({
    initialValues: {
      fecha: "",
      notaSeleccionada: "",
      motivoSeleccionado: "",
      centroFisicoSeleccionado: "",
    },
    onSubmit: (values, { setSubmitting }) => {
      const combinedData = {
        fecha: values.fecha,
        notas: values.notaSeleccionada,
        motivo: values.motivoSeleccionado,
        centroFisico: values.centroFisicoSeleccionado,
      };
      navigate("/ListaEmpleados");

      axios
        .post("http://localhost:3000/visita/", combinedData)
        .then((response) => {
          console.log(response.data);
        })
        .catch((error) => {
          console.log(error);
        })
        .finally(() => {
          setSubmitting(false);
        });
      const visita = response.json();
      setLastVisitId(visita.id);
    },
  });

  return (
    <div className="d-flex">
      <form onSubmit={formik.handleSubmit}>
        {/* <input
        type="text"
        name="notas"
        onChange={formik.handleChange}
        value={formik.values.notas}
      /> */}
        <label className="form-control">
          Fecha
          <input
            className="form-control"
            type="date"
            name="fecha"
            onChange={formik.handleChange}
            value={formik.values.fecha}
          />
        </label>
        <select
          className="form-select"
          name="motivoSeleccionado"
          value={formik.values.motivoSeleccionado}
          onChange={formik.handleChange}
        >
          <option value="">Seleccione un motivo</option>
          {motivoData.map((motivo) => (
            <option key={motivo.id} value={motivo.id}>
              {motivo.nombre}
            </option>
          ))}
        </select>

        <select
          className="form-select"
          name="centroFisicoSeleccionado"
          value={formik.values.centroFisicoSeleccionado}
          onChange={formik.handleChange}
        >
          <option value="">Seleccione un centro físico</option>
          {centroFisicoData.map((centroFisico) => (
            <option key={centroFisico.id} value={centroFisico.id}>
              {centroFisico.nombre}
            </option>
          ))}
        </select>
        <button
          className="btn btn-outline-success  mt-1"
          type="submit"
          disabled={formik.isSubmitting}
        >
          Enviar
        </button>
      </form>
    </div>
  );
}
