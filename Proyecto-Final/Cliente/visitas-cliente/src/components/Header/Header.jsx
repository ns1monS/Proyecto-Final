import { BsBriefcaseFill } from "react-icons/bs";
import { BsFillFilePersonFill } from "react-icons/bs";
import { Link } from "react-router-dom";
import "./Header.css";
import { useAuthContext } from "../../context/AuthContext/AuthContext";
export default function Header() {
  const { logout } = useAuthContext();
  return (
    <div className="container d-flex  py-3 gap-2 h4 justify-content-around  m-0 p-0">
      <button type="button" className="btn btn-none">
        Historial <BsBriefcaseFill />
      </button>
      <Link to="/">
        <button onClick={logout} type="button" className="btn btn-none">
          Logout <BsFillFilePersonFill />
        </button>
      </Link>
    </div>
  );
}
