import React, { useState, useEffect } from "react";
import { useParams, useNavigate, Link } from "react-router-dom";
import axios from "axios";

export default function Empleados() {
  const navigate = useNavigate();
  const [empleados, setEmpleados] = useState([]);

  const verDetalles = (id) => {
    navigate(`/Contacto/${id}`);
  };
  useEffect(() => {
    const obtenerEmpleados = async () => {
      try {
        const response = await axios.get("http://localhost:3000/empleado/");
        setEmpleados(response.data);
      } catch (error) {
        console.error(error);
      }
    };
    obtenerEmpleados();
  }, []);

  return (
    <div>
      <div>
        {empleados.map((empleado) => (
          <div key={empleado.id} className="card">
            <p>Nombre :{empleado.nombre}</p>
            <p>Apellidos :{empleado.apellidos}</p>
            <p>Nif :{empleado.nif}</p>

            <button
              className="btn btn-outline-success"
              onClick={() => verDetalles(empleado.id)}
            >
              Crear Contacto
            </button>
          </div>
        ))}
      </div>
      <div>
        <Link to="/crearEmpleado">
          {" "}
          <button className="btn btn-outline-dark"> Crear empleado</button>
        </Link>{" "}
      </div>
    </div>
  );
}
