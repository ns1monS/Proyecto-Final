import { BsFillHouseDoorFill } from "react-icons/bs";
import { Link } from "react-router-dom";
export default function Footer() {
  return (
    <>
      <Link to="/">
        <button type="button" className="btn btn-none">
          <BsFillHouseDoorFill />
        </button>
      </Link>
    </>
  );
}
