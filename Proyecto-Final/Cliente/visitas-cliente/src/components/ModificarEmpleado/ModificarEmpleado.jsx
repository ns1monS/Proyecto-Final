import { useState, useEffect } from "react";
import { useParams, useNavigate } from "react-router-dom";
import axios from "axios";

export default function ModificarEmpleado(values) {
  const [empleado, setEmpleado] = useState([]);
  const { id } = useParams();
  const navigate = useNavigate();

  useEffect(() => {
    const obtenerEmpleado = async () => {
      try {
        const response = await axios.get(
          `http://localhost:3000/empleado/${id}`
        );
        setEmpleado(response.data);
      } catch (error) {
        console.error(error);
      }
    };
    obtenerEmpleado();
  }, [id]);

  const deleteSubmit = async (event) => {
    event.preventDefault();
    try {
      await axios.delete(`http://localhost:3000/empleado/${id}`, values);
      navigate("/ListaEmpleados");
      alert("Empleado eliminado con exito");
    } catch (error) {
      console.error(error);
    }
  };
  const handleChange = (event) => {
    const { name, value } = event.target;
    setEmpleado({ ...empleado, [name]: value });
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    try {
      await axios.put(`http://localhost:3000/empleado/${id}`);
      navigate("/ListaEmpleados");
      alert("Empleado modificado con exito");
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <>
      <h1>Editar Empleado</h1>
      <div className="d-flex ms-5">
        <div>
          <form onSubmit={handleSubmit}>
            <div>
              <label htmlFor="nombre">Nombre:</label>
              <input
                className=" form-control"
                type="text"
                id="nombre"
                name="nombre"
                value={values.nombre}
                onChange={handleChange}
              />
            </div>
            <div>
              <label htmlFor="nombre">apellidos:</label>
              <input
                className=" form-control"
                type="text"
                id="apellidos"
                name="apellidos"
                value={values.apellidos}
                onChange={handleChange}
              />
            </div>
            <div>
              <label htmlFor="telefono">telefono:</label>
              <input
                className=" form-control"
                type="text"
                id="telefono"
                name="telefono"
                value={values.telefono}
                onChange={handleChange}
              />
            </div>
            <div>
              <label htmlFor="nif">nif:</label>
              <input
                className=" form-control"
                type="text"
                id="nif"
                name="nif"
                value={values.nif}
                onChange={handleChange}
              />
            </div>

            <button type="submit" className="btn btn-outline-success m-4">
              Guardar cambios
            </button>
          </form>
          <form onSubmit={deleteSubmit}>
            <button type="submit" className="btn btn-outline-danger">
              Borrar Empleado
            </button>
          </form>
        </div>
        <div className="container mt-5"></div>
      </div>
    </>
  );
}
