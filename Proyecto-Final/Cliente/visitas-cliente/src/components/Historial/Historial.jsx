import React, { useState } from "react";

function HistorialFormulario() {
  const [historialDatos, setHistorialDatos] = useState([]);

  const guardarDatos = (nuevosDatos) => {
    setHistorialDatos([...historialDatos, nuevosDatos]);
  };

  return (
    <div>
      <h2>Historial de datos del formulario</h2>
      <table>
        <thead>
          <tr>
            <th>Nombre</th>
            <th>centro</th>
            <th>notas</th>
          </tr>
        </thead>
        <tbody>
          {historialDatos.map((datos, index) => (
            <tr key={index}>
              <td>{datos.nombre}</td>
              <td>{datos.centro}</td>
              <td>{datos.notas}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default HistorialFormulario;
