import { useState, useEffect } from "react";
import { useParams, useNavigate } from "react-router-dom";
import axios from "axios";

export default function Contacto() {
  const [empleados, setEmpleado] = useState([]);
  const { id } = useParams();
  const navigate = useNavigate();
  const [notas, setMensaje] = useState("");
  const [idEmpleado, setIdEmpleado] = useState("");
  const [idContacto, setIdContacto] = useState("");
  const [contacto, setContacto] = useState("");

  const handleSubmit = (event) => {
    event.preventDefault();
    axios
      .post(`http://localhost:3000/contacto/`, {
        empleado: id,
        notas: notas,
      })
      .then((response) => {
        console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const todoList = (id) => {
    navigate(`/Tareas/${id}`);
  };
  const verDetalles = (id) => {
    navigate(`/ModificarEmpleado/${id}`);
  };

  useEffect(() => {
    const obtenerContacto = async () => {
      try {
        const response = await axios.get(
          `http://localhost:3000/contacto/${idContacto}`
        );
        setContacto(response.data);
        setIdContacto(contacto.idContacto);
      } catch (error) {
        console.error(error);
      }
    };
    obtenerContacto();

    const obtenerEmpleado = async () => {
      try {
        const response = await axios.get(
          `http://localhost:3000/empleado/${id}`
        );
        setEmpleado(response.data);
        setIdEmpleado(empleados.id);
      } catch (error) {
        console.error(error);
      }
    };
    obtenerEmpleado();
  }, [id, idContacto]);

  return (
    <div>
      <div>
        {empleados.map((empleado) => (
          <div key={empleado.id} className="card">
            <p>Nombre :{empleado.nombre}</p>
            <p>Apellidos :{empleado.apellidos}</p>
            <p>Nif :{empleado.nif}</p>

            <button
              className="btn btn-outline-success"
              onClick={() => verDetalles(empleado.id)}
            >
              Modificar
            </button>
          </div>
        ))}
      </div>
      <div className="card">
        <form onSubmit={handleSubmit}>
          <p>Anotaciones para el contacto</p>
          <input
            className="form-control"
            type="text"
            id="mensaje"
            name="mensaje"
            value={notas}
            onChange={(event) => setMensaje(event.target.value)}
          />
          <button type="submit" className="btn btn-outline-success">
            Enviar
          </button>
        </form>
        <div key={contacto.idContacto}>
          <button
            className="btn btn-outline-success"
            onClick={() => todoList(contacto.idContacto)}
          >
            Lista de tareas
          </button>
        </div>
      </div>
    </div>
  );
}
