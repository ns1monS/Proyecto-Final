import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import axios from "axios";
export default function RegisterForm() {
  const navigate = useNavigate();
  const [formData, setFormData] = useState({
    nombre: "",
    apellidos: "",
    telefono: "",
    nif: "",
    centro: "",
  });

  const handleInputChange = (event) => {
    setFormData({
      ...formData,
      [event.target.name]: event.target.value,
    });
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    navigate("/ListaEmpleados");

    axios
      .post("http://localhost:3000/empleado/", formData)
      .then((response) => {
        console.log(response.data);
        // Add code here to handle successful registration
      })
      .catch((error) => console.error(error));
  };

  return (
    <div className="card">
      <form onSubmit={handleSubmit}>
        <label>
          nombre:
          <input
            className="form-control"
            type="text"
            name="nombre"
            value={formData.nombre}
            onChange={handleInputChange}
          />
        </label>
        <br />
        <label>
          apellidos:
          <input
            className="form-control"
            type="text"
            name="apellidos"
            value={formData.apellidos}
            onChange={handleInputChange}
          />
        </label>
        <br />
        <label>
          nif:
          <input
            className="form-control"
            type="text"
            name="nif"
            value={formData.nif}
            onChange={handleInputChange}
          />
        </label>
        <label>
          centro:
          <input
            className="form-control"
            type="text"
            name="centro"
            value={formData.centro}
            onChange={handleInputChange}
          />
        </label>
        <br />
        <div>
          <button className="btn btn-outline-success m-5" type="submit">
            Register
          </button>
        </div>
      </form>
    </div>
  );
}
