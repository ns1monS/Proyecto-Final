import React, { useState } from "react";
import axios from "axios";

const submitForm = async (data, visita) => {
  try {
    const response = await axios.post(`http://localhost:3000/visita`, data);
    return response.data;
  } catch (error) {
    console.error(error);
    throw error;
  }
};

const Form = () => {
  const [formData, setFormData] = useState({
    centroFisico: "",
    motivo: "",
    fecha: "",
    notas: "",
  });

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = await submitForm(formData, "visita");
    console.log(data);
  };

  const handleChange = (event) => {
    setFormData({
      ...formData,
      [event.target.name]: event.target.value,
    });
  };

  return (
    <form onSubmit={handleSubmit}>
      <label htmlFor="centroFisico">centroFisico</label>
      <input
        id="centroFisico"
        type="text"
        name="centroFisico"
        value={formData.centroFisico}
        onChange={handleChange}
      />
      <label htmlFor="motivo">motivo</label>
      <input
        id="motivo"
        type="text"
        name="motivo"
        value={formData.motivo}
        onChange={handleChange}
      />
      <label htmlFor="notas">notas</label>
      <input
        id="notas"
        type="text"
        name="notas"
        value={formData.notas}
        onChange={handleChange}
      />
      <button type="submit">enviar</button>
    </form>
  );
};
