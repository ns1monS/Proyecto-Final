import React, { useState, useEffect } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
export default function TodoList({ contacto }) {
  const [tasks, setTasks] = useState([]);
  const [newTaskDescription, setNewTaskDescription] = useState("");
  const [newTaskDate, setNewTaskDate] = useState("");
  const [newTaskCompleted, setNewTaskCompleted] = useState(false);

  useEffect(() => {
    axios
      .get(`http://localhost:3000/tarea/${contacto}`)
      .then((response) => {
        setTasks(response.data);
      })
      .catch((error) => {
        console.error(error);
      });
  }, [contacto]);

  const handleSubmit = (event) => {
    event.preventDefault();
    if (newTaskDescription.trim()) {
      axios
        .post("http://localhost:3000/tarea/", {
          contacto: contacto,
          descripcion: newTaskDescription,
          fechaVencimiento: newTaskDate,
          completada: newTaskCompleted,
        })
        .then((response) => {
          setTasks((prevTasks) => [...prevTasks, response.data]);
          setNewTaskDescription("");
          setNewTaskDate("");
          setNewTaskCompleted(false);
        })
        .catch((error) => {
          console.error(error);
        });
    }
  };

  return (
    <div className="card  ">
      <h2>Todo List para empleado {contacto}</h2>
      <form onSubmit={handleSubmit}>
        <div>
          <label>
            Description:
            <input
              className="form-control"
              type="text"
              value={newTaskDescription}
              onChange={(event) => setNewTaskDescription(event.target.value)}
            />
          </label>
        </div>
        <div>
          <label>
            Fecha de vencimiento:
            <input
              className="form-control"
              type="date"
              value={newTaskDate}
              onChange={(event) => setNewTaskDate(event.target.value)}
            />
          </label>
        </div>
        <div>
          <label>
            completada:
            <input
              className="text-center"
              type="checkbox"
              checked={newTaskCompleted}
              onChange={(event) => setNewTaskCompleted(event.target.checked)}
            />
          </label>
        </div>
        <button type="submit" className="btn btn-outline-success">
          Añadir tarea
        </button>
      </form>
      <div>
        <Link to="/ListaEmpleados">
          <button className="btn">Atras</button>
        </Link>
      </div>

      {/* ... */}
    </div>
  );
}
