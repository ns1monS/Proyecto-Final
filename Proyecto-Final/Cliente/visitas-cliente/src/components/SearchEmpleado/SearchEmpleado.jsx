import React, { useState } from "react";
import axios from "axios";
import "./search.css";

export default function searchEmpleado() {
  const [empleado, setEmpleado] = useState("");
  const [resultados, setResultados] = useState([]);

  const handleChange = (event) => {
    setEmpleado(event.target.value);
    axios
      .get(`http://localhost:3000/empleado/nif/${empleado}`)
      .then((response) => {
        console.log("esto es response", response);
        setResultados(response.data);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  return (
    <div className="main">
      <div>
        <div>
          <label>Buscar empleado:</label>
        </div>
        <input
          type="search"
          id="nif"
          name="nif"
          placeholder="nif"
          value={empleado}
          onChange={(event) => setEmpleado(event.target.value)}
        />

        <button onClick={handleChange}>Buscar</button>
      </div>

      {resultados ? (
        <div>
          <button onClick={() => console.log("crear visita")}>
            crear visita
          </button>
          <button onClick={() => console.log("Modificar empleado")}>
            Modificar empleado
          </button>
        </div>
      ) : (
        <button onClick={() => console.log("Crear empleado")}>
          Crear empleado
        </button>
      )}
    </div>
  );
}
